#!/usr/bin/env python3
# -*- coding: utf-8 -*-

''' Example for managing spots collectively with the Network class

Demostration of PWM at several frequencies and all channels
turning ON asynchronously
'''

# Written by Anthony Fratamico, PhD at University of Liège (Belgium)
# Laboratory of Plant Physiology
# Writing date: 2020-08-31

# Libraries
import sys
import phs16 as dev
from time import sleep

# Spots addresses
spots_add = [140, 142, 150] # change addresses according to your devices

# Serial port
serial_port = "/dev/ttyUSB0" # change path according to your system

# Create and initialize a communication object
com = dev.Com(port=serial_port)
err = com.connexion()
if err != 0:
	print("Unable to open port")
	sys.exit(1)

# Create a network object
net = dev.Network(com=com, spots=spots_add)

# Initialize the network
err = net.activate()
if err != 0:
	print("Unable to activate spot")
	sys.exit(1)

# Considering all light spots is called, give the role of master automatically
# and set power OFF
# and and frequency to 0.5 Hz
err = net.set_config(master = -1, power = 0, freq = 0.5)
if err != 0:
	print("Error during set_config")

# Check the configuration
check = net.check()
if check != 0:
	print("Configuration of network not valid !")
	sys.exit(1)

# Ask for available LED colors
colors = net.get_infos(what="available_colors")
colors = colors['available_colors']

# Setting PWM: colors asynchronously
net.shutdown()
i = 0
for col in colors:
	# Turn the color at 10% of max current
	val = 5.00
	net.set_colors(colors=col, intensity=val,
		pwm_start=i / len(colors),
		pwm_stop=(i+1) / len(colors))
	i += 1

# Test: LED drivers ON during 20s
print("Start ...")
net.set_config(power = 1)
sleep(20)
net.set_config(power = 0)
print("... Done !")

# Close the port and exit
com.close()
sys.exit()