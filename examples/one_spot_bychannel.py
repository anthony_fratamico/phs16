#!/usr/bin/env python3
# -*- coding: utf-8 -*-

''' Example for managing spots individually with the Spot class

Demostration of a control of each channel one by one and print
LED description
'''

# Written by Anthony Fratamico, PhD at University of Liège (Belgium)
# Laboratory of Plant Physiology
# Writing date: 2020-08-31

# Librarires
import sys
import phs16 as dev
from time import sleep

# Spot addresse
spot_add = 140 # change address according to your device

# Serial port
serial_port = "/dev/ttyUSB0" # change path according to your system

# Create and initialize a communication object
com = dev.Com(port=serial_port)
err = com.connexion()
if err != 0:
	print("Unable to open port")
	sys.exit(1)

# Create and initialize a spot object
spot = dev.Spot(address=spot_add, com=com)
err = spot.activate()
if err != 0:
	print("Unable to activate spot")
	sys.exit(1)

# Considering only one light spot on the network, give it the role of master
# and set power ON
err = spot.set_config({'master': 1, 'power': 1})
if err != 0:
	print("Error during set_config")

# Ask for channel description (output is a dictionnary)
channels_descr = spot.get_specs()

# Select the description of channels on the first PCB
channels_descr = channels_descr['pcb-led'][0]['desc']['channels']

# Test: Turn on channels, one by one, and print their description
print("Start ...")
spot.shutdown()
for ch_id, ch_descr in channels_descr.items():
	# Print channel description
	print("Channel ID: "+str(ch_id))
	for key, value in ch_descr.items():
		print("   "+key+": "+str(value))
	# Turn the channel at 5% of max current
	val = 5
	spot.set_channels(channels=ch_id, intensity=val)
	# Sleep
	sleep(2)

# End test
spot.shutdown()
print("... Done !")

# Close the port and exit
com.close()
sys.exit()