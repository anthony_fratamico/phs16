#!/usr/bin/env python3
# -*- coding: utf-8 -*-

''' Example for managing spots collectively with the Network class

Demostration of a control of each LED type, one by one
'''

# Written by Anthony Fratamico, PhD at University of Liège (Belgium)
# Laboratory of Plant Physiology
# Writing date: 2020-08-31

# Libraries
import sys
import phs16 as dev
from time import sleep

# Spots addresses
spots_add = [140, 142, 150] # change addresses according to your devices

# Serial port
serial_port = "/dev/ttyUSB0" # change path according to your system

# Create and initialize a communication object
com = dev.Com(port=serial_port)
err = com.connexion()
if err != 0:
	print("Unable to open port")
	sys.exit(1)

# Create a network object
net = dev.Network(com=com, spots=spots_add)

# Search spot on network
out = net.find_spots()
if len(out['not-found']) != 0:
	print("Following spots are not found: "+str(out['not-found']))

# Initialize the network
err = net.activate()
if err != 0:
	print("Unable to activate spot")
	sys.exit(1)

# Considering all light spots is called, give the role of master automatically
# and set power ON
# and set frequency to 50Hz
err = net.set_config(master = -1, power = 1, freq = 50)
if err != 0:
	print("Error during set_config")

# Check the configuration
check = net.check()
if check != 0:
	print("Configuration of network not valid !")
	sys.exit(1)

# Ask for available LED colors (output is a dictionnary)
colors = net.get_infos(what="available_colors")

# Test: Run a sequence
print("Start ...")
for col in colors['available_colors']:
	# Shutdown
	net.shutdown()
	# Turn the color at 10% of max current
	val = 10.00
	print("LED "+col+" at "+str(val)+"%")
	err = net.set_colors(colors=col, intensity=val)
	if err != 0:
		print("Error during set_colors")
	# Sleep
	sleep(2)

# End test
net.shutdown()
print("... Done !")

# Close the port and exit
com.close()
sys.exit()