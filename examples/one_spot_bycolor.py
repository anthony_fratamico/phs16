#!/usr/bin/env python3
# -*- coding: utf-8 -*-

''' Example for managing spots individually with the Spot class

Demostration of a control of each LED type one by one
'''

# Written by Anthony Fratamico, PhD at University of Liège (Belgium)
# Laboratory of Plant Physiology
# Writing date: 2020-08-31

# Libraries
import sys
import phs16 as dev
from time import sleep

# Spot addresse
spot_add = 140 # change address according to your device

# Serial port
serial_port = "/dev/ttyUSB0" # change path according to your system

# Create and initialize a communication object
com = dev.Com(port=serial_port)
err = com.connexion()
if err != 0:
	print("Unable to open port")
	sys.exit(1)

# Create and initialize a spot object
spot = dev.Spot(address=spot_add, com=com)
err = spot.activate()
if err != 0:
	print("Unable to activate spot")
	sys.exit(1)

# Considering only one light spot on the network, give it the role of master
# and set driver power ON
err = spot.set_config(config={'master': 1, 'power': 1})
if err != 0:
	print("Error during set_config")

# Ask for available LED colors
colors = spot.get_available_colors()

# Test: Turning ON each color, one at a time
print("Start ...")
for col in colors:
	# Shutdown
	spot.shutdown()
	# Turn the color at 10% of max current
	val = 10.00
	print("LED "+col+" at "+str(val)+"%")
	err = spot.set_colors(colors=col, intensity=val)
	if err != 0:
		print("Error during set_colors")
	# Sleep
	sleep(2)

# End of test
spot.shutdown()
print("... Done !")

# Close port and exit
com.close()
sys.exit()