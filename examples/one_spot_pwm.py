#!/usr/bin/env python3
# -*- coding: utf-8 -*-

''' Example for managing spots individually with the Spot class

Demostration of PWM at several frequencies and all channels
turning ON asynchronously
'''

# Written by Anthony Fratamico, PhD at University of Liège (Belgium)
# Laboratory of Plant Physiology
# Writing date: 2020-08-31

# Libraries
import sys
import phs16 as dev
from time import sleep

# Spot addresse
spot_add = 140 # change address according to your device

# Serial port
serial_port = "/dev/ttyUSB0" # change path according to your system

# Create and initialize a communication object
com = dev.Com(port=serial_port)
err = com.connexion()
if err != 0:
	print("Unable to open port")
	sys.exit(1)

# Create and initialize a spot object
spot = dev.Spot(address=spot_add, com=com)
err = spot.activate()
if err != 0:
	print("Unable to activate spot")
	sys.exit(1)

# Considering only one light spot on the network, give it the role of master
err = spot.set_config(config={'master': 1})
if err != 0:
	print("Error during set_config")

# Ask for channel description (output is a dictionnary)
channels_descr = spot.get_specs()

# Select the list of ID channels on the first PCB
channels_id = list(channels_descr['pcb-led'][0]['desc']['channels'].keys())
channels_number = len(channels_id)

# Turn on all channels, asynchronously
spot.shutdown()
spot.set_config({'power': 0}) # turn drivers OFF during PWM setting (optionnal)
for i in channels_id:
	val = 5
	err = spot.set_channels(channels=i, intensity=val,
		pwm_start=i / channels_number,
		pwm_stop=(i+1) / channels_number)
	if err != 0 :
		print("Error during set_channels (channel ID: "+str(i)+")")

# Test: Change frequency
print("Start ...")
freqs = [0.2, 0.5,1,5,10,25,50,2500]
spot.set_config({'power': 1}) # turn drivers ON
for freq in freqs:
	print("F = "+str(freq)+" Hz")
	err = spot.set_config({'freq': freq})
	if err != 0 :
		print("Error during set_config")
	sleep(3/freq + 5) # almost 3 cycles + 5 seconds

# End test
spot.shutdown()
print("... Done !")

# Close the port and exit
com.close()
sys.exit()