# Lumiatec PHS16

Lumiatec PHS16 is a LED based lighting developped for plant biology research. The library **phs16** provide a set of classes and methods to communicate easily with a unlimited number of lightings to control them.

## Prerequisites

Following python packages are required:

* os
* logging
* datetime
* copy
* itertools
* serial
* time (in script examples)

## Features

phs16 provide

* a class *Com* for the communication trought a USB-RS485 converter
* a class *Spot* to control lighting
* a class *Network* to gather Spot'objects and control a set of lightings together

## Example

See examples'folder for some examples of library use.

This is a minimal example:

```
import phs16 as mod

con = mod.Com(port="/dev/ttyUSB0") # change port according to your system/product
con.connexion() # intialize the communication pipe

net = mod.Network(com=con, spots=[250,281,302]) # passing the com'object and the spots list (depending of your products)
net.activate() # building the network

net.set_colors(colors="*", intensity=50.0) # turning all LED at 50% power
net.shutdown() # turning off all LED

con.close() # close the communication pipe
```

## Built With

* [Python 3.5.2](https://www.python.org/) - Write and test
* [pdoc](https://github.com/mitmproxy/pdoc) - Generate documentation
* [pylint](https://www.pylint.org/) - Clean the code


## Versioning

* **0.0.0**: developpement tests
* **0.1.0**: first complete version

## Authors

* **Anthony Fratamico**

## License

Writing in course...
