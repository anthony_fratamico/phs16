""" Module Lumiatec PHS16

Fournit l'ensemble des classes et fonctions pour contrôler
les luminaire Lumiatec, modèle PHS-16
"""

__author__ = "Anthony Fratamico"
__version__ = "0.1.1"
__license__ = ""

# Written by Anthony Fratamico, PhD at University of Liège (Belgium)
# Laboratory of Plant Physiology
# Developpement initialized: 2020-06-11
# Last revision: 2020-08-20

# ============================================================================
# BIBLIOTHÈQUES
# ----------------------------------------------------------------------------
import os
import logging
from datetime import datetime
import copy
from itertools import repeat
import serial
import serial.tools.list_ports
# ============================================================================
# LOGGING # Adapté de https://docs.python.org/2/howto/logger-cookbook.html
# ----------------------------------------------------------------------------
# Logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG) # par défaut
# ----------------------------------------------------------------------------
def set_log(verbosity, file=None, quiet=False):
    """Réglage du degré de verbosité des logs

    Parameters
    ----------
    verbosity: int
        logging.CRITICAL,
        logging.ERROR,
        logging.WARNING,
        logging.INFO,
        logging.DEBUG
    file: str
        chemin vers le fichier de log.
        None (défaut) évite l'enregistrement des logs
        dans un fichier
    quiet: bool
        active (False, défaut) ou supprime (True) l'affichage
        des logs sur la sortie standard.

    Returns
    -------
    int
        0 (succès) ou 1 (erreur)

    """

    if quiet is False:
        std_log = logging.StreamHandler()
        std_log.setLevel(verbosity)
        std_log.setFormatter(logging.Formatter(\
            '[%(levelname)s] (%(name)s:%(funcName)s) %(message)s'))
        logger.addHandler(std_log)
    if file is not None:
        # Crèe le dossier si n'existe pas
        if (os.path.dirname(file) != "") and\
        (not os.path.isdir(os.path.dirname(file))):
            try:
                os.mkdir(os.path.dirname(file))
            except:
                print("Unable to create directory for log file")
                return 1
        # Logger
        file_log = logging.FileHandler(file)
        file_log.setLevel(verbosity)
        file_log.setFormatter(logging.Formatter(\
            '%(asctime)s :: %(name)s:%(funcName)s :: %(levelname)s :: %(message)s'))
        logger.addHandler(file_log)
    return 0

# valeur par défaut
set_log(logging.WARNING)
# ============================================================================
# VARIABLES GÉNÉRALES
# ----------------------------------------------------------------------------
date_format = "%Y-%m-%d %H:%M:%S.%f"
# ============================================================================
# FONCTIONS
# ----------------------------------------------------------------------------
def debugcom():
    """Communication directe avec le port série.

    Returns
    -------
    None

    Notes
    -----
    Propose les paramètres par défaut pour l'ouverture du port
    à valider (ENTER) ou modifier.
    L'entête et la checksum sont automatiquement ajoutés.

    See Also
    --------
    Com

    Example
    -------
    Pour interroger la version du firmware du spot 40,
    entrer "GL 40" + ENTER.
    Pour quiiter, taper Ctrl+C.
    """

    logger.debug("CALL")
    # Configuration du port
    serinfos = {
        'port': "/dev/ttyUSB0",
        'baudrate': "115200",
        'timeout': "1"
    }
    print("Port configuration (press ENTER to accept default values)")
    for k in ['port', 'baudrate', 'timeout']:
        try:
            ask = input("   "+k+" [default: "+str(serinfos[k])+"]: ")
            if ask != "":
                serinfos[k] = str(ask)
        except KeyboardInterrupt:
            print("\nEXIT")
            return
    serinfos['baudrate'] = int(serinfos['baudrate'])
    serinfos['timeout'] = float(serinfos['timeout'])
    logger.debug("Serial port configuration: %s", serinfos)
    # Ouverture du port
    com = Com(port=serinfos['port'],
              baudrate=serinfos['baudrate'],
              timeout=serinfos['timeout'])
    error = com.connexion()
    if error != 0:
        logger.critical("Unable to open serial port")
        return
    # Communication
    print("Communication in autocomplete mode")
    while True:
        try:
            command = input("COMMAND : ")
            cmd = command.split(" ")[0]
            add = command.split(" ")[1]
            arg = " ".join(command.split(" ")[2:])
            out = com.send(cmd=cmd, prefix=add, arg=arg)
            logger.debug("Reply: %s", out)
            print("   SENDED: " + out['cmd'])
            print("   REPLY: " + out['raw'])
        except KeyboardInterrupt:
            logger.debug("Keyboard interrupt received")
            print("\nEXIT")
            break
    return
# ----------------------------------------------------------------------------
def decrypt_led(string, length, coding):
    """Décrypte le descriptif détaillé des LEDs

    Parameters
    ----------
    string: str
        chaîne de caractères à décrypter
    length: int
        nombre de caractère à décoder
    coding: str
        type à renvoyer: text, in ou hex

    Returns
    -------
    tuple
        Permier élément: les données décryptées
        Second élément: reste de la chaîne de caractères
        non décryptée.

    See Also
    --------
    Spot.read_specs()

    """

    # Subset
    data = string[0:int(2*length/8)]
    remainder = string[int(2*length/8):]
    # Division par 2 charactères
    data = [data[i:i+2] for i in range(0, len(data), 2)]
    # LSB first ?
    if length > 8:
        data.reverse()
    # Type
    data = "".join(data)
    if coding == "text":
        try:
            data = bytearray.fromhex(data).decode()
        except:
            data = None
    elif coding == "int":
        try:
            data = int(data, 16)
        except ValueError:
            data = None
    elif coding == "hex":
        try:
            data = "0x"+data
        except TypeError:
            data = None
    return data, remainder
# ----------------------------------------------------------------------------
def convert(data, type_out):
    """Converti une donnée dans un autre type.

    Parameters
    ----------
    data: str, int, float, list
        donnée à convertir (list, str, float, int...)
    type_out: type
        type de destination

    Returns
    -------
    int, str, list...
        la donnée convertie au type demandé ou None si
        la convertion a échoué.

    Examples
    --------
    - convert(data="8", type_out=int)
    - convert(data="BL_460", type_out=list)
    """

    try:
        out = type_out(data)
    except (ValueError, TypeError):
        out = None

    return out

def convert_tolistof(data, type_out, none_omit=False):
    """Converti les données suivant le type demandé
    et les regroupe dans une liste.

    Parameters
    ----------
    data: str, int, float, list
        donnée à convertir (list, str, float, int...)
    type_out: type
        type de destination
    none_omit: bool
        supprime (True) les valeurs 'None' de la liste
        ou les conserve (False, défaut)

    Returns
    -------
    list
        données convertie

    See Also
    --------
    convert

    Examples
    --------
    - convert(x=["8", 25, "dix", 78.2, "33.3", "vingt"],
    type_out=int,
    none_omit=False)
    - convert(x=["8", 25, "dix", 78.2, "33.3", "vingt"],
    type_out=int,
    none_omit=True)
    - convert(x=["YE_590", 89], type_out=str)

    """

    if not isinstance(data, list):
        data = [data]
    out = list(map(convert, data, repeat(type_out)))
    if none_omit:
        out = list(filter(None.__ne__, out))

    return out
# ----------------------------------------------------------------------------
def upload_firmware(spot, port, file):
    """Met à jour le firware d'un luminaire

    Parameters
    ----------
    spot:
        adresse du spot à mettre à jour
    port: str
        chemin vers le port série
    file: str
        chemin vers le fichier binaire de la nouvelle version
        du firmware

    Returns
    -------
    int
        0 (succès) ou 1 (erreur)

    Notes
    -----
    En développement.

    """

    logger.debug("CALL")
    # Vérification du spot
    try:
        spot = int(spot)
    except ValueError:
        logger.error("Spot address must be an integer")
        return 1
    # Port série
    logger.debug("Initialize serial port")
    com = Com(port=port)
    error = com.connexion()
    if error != 0:
        return 1
    # Recherche du spot
    logger.debug("Test addresse")
    reply = com.send("GI", prefix=spot)
    if reply['reply'] == "":
        logger.debug("   ... address not found")
        logger.error("Address not found")
        return 1
    old_version = reply['reply']
    logger.debug("   ... found. Current version: %s", old_version)
    # Chargement du module XMODEM
    logger.debug("Import module XMODEM")
    try:
        from xmodem import XMODEM
    except ImportError:
        logger.critical("Unable to load module XMODEM")
        return 1
    def getc(size):
        return com.ser.read(size) or None
    def putc(data):
        return com.ser.write(data)
    modem = XMODEM(getc, putc)
    try:
        stream = open(file, 'rb')
    except:
        logger.critical("Unable to open file")
        return 1
    # Préparation du Spot
    logger.debug("Initialization of reception")
    reply = com.send(cmd="UP", prefix=spot, arg=1)
    if reply['reply'] == "ACK":
        logger.debug("   ... ready")
    else:
        logger.debug("   ... failed !")
        logger.error("unable to initialize reception")
        return 1
    # Envoi
    logger.debug("Sending")
    reply = modem.send(stream)
    if reply is True:
        logger.debug("   ...done")
    else:
        logger.debug("   ... failed !")
        logger.debug("   ... restart CPU")
        reply = com.send(cmd="RST", prefix=spot)
        return 1
    # Flash
    logger.debug("Flashing")
    reply = com.send(cmd="FF", prefix=spot)
    print(reply)
    # Restart
    logger.debug("Restart CPU")
    reply = com.send(cmd="RST", prefix=spot)
    print(reply)
    # Check nouvelle version
    logger.debug("Checking version")
    reply = com.send("GI", prefix=spot)
    logger.debug("  ... new version: %s", reply['reply'])
    return 0

# ============================================================================
# CLASSES
# ----------------------------------------------------------------------------
class Com():
    """Classe pour la communication série/RS485.
    Transmise aux classes Network, Spot et Channel.
    
    ...
    
    Attributes
    ----------
    port : str
        string du chemin vers le port série (par ex. /dev/ttyUSB0)
    baudrate: int
        baudrate (defaut 115200)
    timeout: int
        timeout pour la communication (defaut 1 seconde)
    """

    def __init__(self, port, baudrate=115200, timeout=1):
        """
        Parameters
        ----------
        port: str
            chemin vers le port série
        baudrate: int
            baudrate de la communication série (défaut 115200)
        timeout: int
            timetout de la communication série (défaut 1 seconde)
        """

        self.port = port
        self.baudrate = baudrate
        self.timeout = timeout
        self.ser = None

    def get_ports(self):
        """Renvoi la liste des ports disponibles

        Charge le module serial.tools.list_ports

        Returns
        -------
        list
            liste des ports disponibles
        """

        logger.debug("CALL")
        available_ports = []
        for port in  list(serial.tools.list_ports.comports()):
            port = tuple(port)
            available_ports.append(port[0])
        return available_ports

    def connexion(self):
        """Initie la connexion série

        Returns
        -------
        int
            0 (succes) or 1 (error)
        """

        logger.debug("CALL")
        # Vérification de la présence du port
        if self.port not in self.get_ports():
            logger.critical("Port %s not found", self.port)
            return 1
        # Connexion
        try:
            self.ser = serial.Serial(port=self.port, baudrate=self.baudrate, timeout=self.timeout)
            self.ser.isOpen()
            logger.debug("Port is opened")
            return 0
        except IOError:
            logger.critical("Unable to open port")
            return 1

    def open(self):
        """Ouvre le port série

        Returns
        -------
        int
            0 (succes) or 1 (error)
        """

        if not self.ser.isOpen():
            try:
                self.ser.open()
            except:
                logger.error("Unable to open port")
                return 1
        return 0

    def close(self):
        """Ferme le port série

        Returns
        -------
        int
            0 (succes) or 1 (error)
        """

        if self.ser.isOpen():
            try:
                self.ser.close()
            except:
                logger.error("Unable to close port")
                return 1
        return 0

    def compute_checksum(self, string):
        """Calcul la somme de contrôle

        Parameters
        ----------
        string: str
            chaine de caractère dont la somme de contrôle doit être calculée

        Returns
        -------
        int
            checksum
        """

        checksum = 0
        for char in list(string):
            checksum = checksum + ord(char)
        return checksum % 256

    def send(self, cmd, prefix="", arg="", checksum=True):
        """Envoie un message sur le port série et écoute la réponse

        Parameters
        ----------
        cmd: str
            commande à envoyer
        prefix: list
            chaînes de caractères à ajouter avant la commande
        arg: list
            arguments à ajouter après la commande
        checksum: bool
            vérifie (True, défaut) ou pas (False) la communication

        Returns
        -------
        dict
            rapport détaillé de la communication
        """

        #logger.debug("CALL")
        # Dictionnaire pour la sortie
        reply_dict = {
            'raw': None,
            'cmd': None,
            'checksum': None,
            'reply': None,
            'log': None,
            'error': True}
        # Vérifie que le port est ouvert
        error = self.open()
        if error != 0:
            reply_dict['log'] = "Unable to open serial port"
            logger.error(reply_dict['log'])
            return reply_dict
        # Préparation de la commande balisée
        prefix = convert_tolistof(data=prefix, type_out=str)
        prefix = " ".join(prefix)
        arg = convert_tolistof(arg, type_out=str)
        arg = " ".join(arg)
        cmdbal = str("@"+str(cmd)+" "+prefix+" "+arg)
        if checksum:
            cmdbal = cmdbal+"*"+str(self.compute_checksum(cmdbal))
        cmdbal += "\r"
        reply_dict['cmd'] = cmdbal
        # Envoie de la commande
        self.ser.write(cmdbal.encode()) # encodage ascii nécessaire
        # Écoute de la réponse
        reply = self.ser.read_until(str("\r").encode())
        self.ser.flushInput() # flush, utile si timeout dépassé
        self.ser.flushOutput()
        # Sauvegarde de la réponse brute
        reply_dict['raw'] = str(reply)
        # Decodage simple
        head = "#"+cmd+" "
        queue = "*"
        cleaning = False
        try:
            reply = reply.decode()
            reply = reply.lstrip("\x00") # bytes nulle reçu avant. Signe de ligne poluée?
        except:
            cleaning = True
        # Analyse de la réponse
        if cleaning is False:
            reply = reply.split("\r", 1)[0]
            if (reply.startswith(head)) and (queue in reply): # réponse structurée
                reply_dict['reply'] = reply.split(head, 1)[1] # supprime l'entête
                reply_dict['reply'] = reply_dict['reply'].split(queue, 1)[0]
                reply_dict['checksum'] = self.compute_checksum(reply.split(queue, 1)[0])
                if reply_dict['checksum'] == int(reply.split(queue, 1)[1], 16):
                    reply_dict['error'] = False
                else:
                    reply_dict['log'] = "Wrong checksum"
            elif reply == "ACK" or reply.startswith("NAK"):
                reply_dict['reply'] = reply
                reply_dict['error'] = False
            else:
                reply_dict['reply'] = ""
                reply_dict['log'] = "Wrong reply structure"
        else:
            logger.warning("Unexpected bytes received. Trying to clean.")
            # décodage en cas de ligne poluée et réception de bytes en dehors de la réponse
            reply = reply.split("\r".encode(), 1)[0]
            if (head.encode() in reply) and (queue.encode() in reply): # réponse structurée
                reply = reply.split(head.encode(), 1)[1] # supprime l'entête et avant
                checksum = reply.split(queue.encode(), 1)[1]
                reply = reply.split(queue.encode(), 1)[0]
                try:
                    reply_dict['reply'] = reply.decode()
                    checksum = int(checksum.decode(), 16)
                except:
                    reply_dict['reply'] = ""
                    reply_dict['log'] = "Unable to decode reply string even after cleanning"
                    return reply_dict
                reply_dict['checksum'] = self.compute_checksum(head+reply_dict['reply'])
                if reply_dict['checksum'] == checksum:
                    reply_dict['error'] = False
                else:
                    reply_dict['log'] = "Wrong checksum"
            elif "ACK".encode() in reply:
                reply_dict['reply'] = "ACK"
                reply_dict['error'] = False
            elif "NAK ".encode() in reply:
                try:
                    reply_dict['reply'] = "NACK "+str(int(reply.split("NACK ".encode(), 1)[1]))
                    reply_dict['error'] = False
                except:
                    reply_dict['reply'] = "NACK"
                    reply_dict['log'] = "Unable to decode NACK code even after cleaning"
            else:
                reply_dict['reply'] = ""
                reply_dict['log'] = "Wrong reply structure"
        # Sortie
        if reply_dict['error']:
            logger.error("Com. error: %s", reply_dict['log'])
        return reply_dict

# ----------------------------------------------------------------------------
class Network():
    """Classe pour la gestion d'un réseau de spots.
    Un réseau regroupe un ensemble de spots paramétrables de la classe 'Spot'.
    Tous les spots d'un même réseau doivent être réglés à la même fréquence
    de PWM. Un spot doit avoir le rôle de master, tous les autres celui
    d'esclave. La classe Network assure ce genre de contrôle.

    ...
    
    Attributes
    ----------
    com: Com
        objet de la classe Com() pour la communication avec les luminaires
    spots_input: list of int
        adresses des spots sur le réseau
    spot_add: list of int
        adresses des spots trouvés sur le réseau
    spots: dict
        ensemble des objets de la classe Spot
    available_color: list of str
        liste des LED disponibles sur le réseau
    activ: date
        horodatage de l'activation du réseau
    """

    def __init__(self, com, spots):
        """
        Parameters
        ----------
        com: Com
            objet de la classe Com() pour la communication avec le luminaire
        spots: list of int
            liste des adresses des spots
        """

        logger.debug("CALL")
        self.com = com
        # Spots
        self.spots_input = spots
        self.spots_add = None
        self.spots = {}
        self.available_color = []
        self.activ = None

    def find_spots(self, spots=None):
        """Test les adresses des spots

        Parameters
        ----------
        spots: list of int
            adresses à tester. La valeur par défaut est la liste
            de l'attribut spot_input de la classe.

        Returns
        -------
        dict
            dictionnaire reprendant la liste des luminaires
            présents sur le réseau (sous la clé 'found') ou
            absents (clé 'not-found').

        Notes
        -----
        La commande de version du firmware du luminaire est
        envoyée à chaque adresse. En l'absence de réponse (timeout),
        l'adresse est jugée absente du réseau.
        """

        logger.debug("CALL")
        if spots is None:
            spots = []
        if len(spots) == 0:
            spots = self.spots_input
        spots = convert_tolistof(data=spots, type_out=int, none_omit=True)
        out = {'found': [], 'not-found':[]}
        for add in spots:
            reply = self.com.send(cmd="GI", prefix=add)
            if reply['error'] is False and reply['reply'] != "":
                out['found'].append(int(add))
            else:
                out['not-found'].append(int(add))
        return out

    def activate(self):
        """Teste les adresses des luminaires et initialise le réseau
        en créant les objets de classe 'Spot' pour les adresses
        trouvées.

        Returns
        -------
        int
            0 (succès) ou 1 (erreur)

        See Also
        --------
        find_spots()

        Notes
        -----
        Crèe un dictionnaire sous l'attribut 'spots' ayant pour clés
        les adresses valides et comme valeur les objets
        de classe Spot().
        """

        logger.debug("CALL")
        spots_result = self.find_spots()
        if len(spots_result['found']) == 0:
            logger.error("No spot found")
            return 1
        if len(spots_result['not-found']) > 0:
            logger.warning("Following spots not found: %s", spots_result['not-found'])
        self.spots_add = list(set(spots_result['found']))
        self.activ = datetime.now().strftime(date_format)

        error = 0
        for add in self.spots_add:
            spot = Spot(add, self.com)
            error += spot.activate()
            self.spots[add] = spot
            self.available_color.extend(spot.get_available_colors())
        self.available_color = list(set(self.available_color))

        if error != 0:
            return 1
        return 0

    def close(self):
        """Ferme le canal de communication

        Returns
        -------
        int
            0 (succès) ou 1 (erreur)

        See Also
        --------
        Com
        """

        error = self.com.close()
        return error

    def set_config(self, master=None, power=None, freq=None, overdrive=None):
        """Règle la configuration commune aux spots du réseau.

        Parameters
        ----------
        master: int
            adresse du spot choisi pour le rôle de maître.
            Une valeur négative sélectionne automatiquement le luminaire
            du réseau ayant l'adresse la plus petite.
            None (défaut) évite la configuration des
            rôles maître/esclave.
        power: int
            0 = inactive les drivers, 1 = active les drivers
            None (défaut) évite la configuration de
            l'activation des drivers
        freq: float
            fréquence (en Hz) du PWM.
            None (défaut) évite la configuration de la fréquence
        overdrive: int
            0 = limite le courant moyen à 50% du courant pic,
            1 = limite le courant moyen à 75% du courant pic,
            2 = aucune limitation du courant,
            None (défaut) évite la configuration de l'overdrive.

        Returns
        -------
        int
            0 (succès) ou 1 (erreur)
        """

        if len(self.spots.keys()) == 0:
            logger.error("No spot defined")
            return 1
        config = {}
        # Master
        if master is not None:
            try:
                master = int(master)
            except ValueError:
                logger.error("Argument 'master' must be an integer")
                return 1
            if master < 0:
                master = min(self.spots.keys())
                logger.info("Master choosen automatically at address %s", master)
            if master not in list(self.spots.keys()):
                logger.error("Address given for master not found")
                return 1
        # Power
        if power is not None:
            if power in [0, 1]:
                config['power'] = power
            else:
                logger.error("Power value not valid")
                return 1
        # Fréquence
        if freq is not None:
            try:
                config['freq'] = float(freq)
            except ValueError:
                logger.error("Frequence must be a float")
                return 1
        # Overdrive
        if overdrive is not None:
            if overdrive in [0, 1, 2]:
                config['overdrive'] = overdrive
            else:
                logger.error("Overdrive value not valid")
                return 1
        # Envoi
        if len(config) == 0:
            return 0
        error = 0
        for key, spot in self.spots.items():
            if master is not None:
                if key == master:
                    config['master'] = 1
                else:
                    config['master'] = 0
            error += spot.set_config(config=config)
        # Sortie
        if error > 0:
            return 1
        return 0

    def check(self, update=True):
        """Vérifie la validité de la configuration du réseau et
        l'état des drivers LED.

        Parameters
        ----------
        update: bool
            True = intérroge les spots, False = établi le
            diagnostic sur base des dernières informations en mémoire.

        Returns
        -------
        int
            0 (configuration valide et driver en fonction)
            ou 1 (configuration invalide et/ou au moins un drivers
            hors-service).

        See Also
        --------
        Network.get_infos()
        """

        master = []
        freq = []
        config = []
        drivers = []
        for spot in self.spots.values():
            conf = spot.get_config(update=update)
            master.append(conf['master'])
            freq.append(conf['freq'])
            status = spot.get_status(update=update)
            if status['config'] == "OK":
                config.append(0)
            else:
                config.append(1)
            if status['drivers']['global'] == "OK":
                drivers.append(0)
            else:
                drivers.append(1)
        error = 0
        if len(master) != 1:
            error += 1
        if len(set(freq)) != 1:
            error += 1
        if sum(config) > 0:
            error += 1
        if sum(drivers) > 0:
            error += 1
        # Sortie
        if error != 0:
            return 1
        return 0

    def get_infos(self, what=None, spots=None, update=False):
        """Renvoie les informations demandées

        Parameters
        ----------
        what: list of string
            clés du dictionnaire à renvoyer. Une liste vide (défaut)
            renvoie la totalité des données disponibles.
        spots: list of int
            liste des adresses des spots à interroger. Une liste
            vide (défaut) renvoie les informations pour l'ensemble
            des spots du réseau.
        update: bool
            True = intérroge les spots, False = renvoie les dernières
            valeurs gardées en mémoire.

        Returns
        -------
        dict
            données classés par type et adresse de spot

        Notes
        -----
        Structure du dictionnaire renvoyé:

        - available colors (list):
        code des LEDs disponibles sur le réseau.
        - temp (dict):
        dictionnaire avec les adresses des spots comme clés et
        les informations de température comme valeur
        (voir Spot.read_temp() et Spot.get_temp() pour plus
        d'informations)
        - status (dit):
        dictionnaire avec les adresses des spots comme clés et
        les informations de status comme valeurs
        (voir Spot.read_status() et Spot.get_status() pour plus
        d'informations)
        - config (dit):
        dictionnaire avec les adresses des spots comme clés et
        leur configuration comme valeurs
        (voir Spot.read_config() et Spot.get_config() pour plus
        d'informations)
        - specs (dit):
        dictionnaire avec les adresses des spots comme clés et
        leurs spécifications comme valeurs
        (voir Spot.read_specs() et Spot.get_specs() pour plus
        d'informations)
        - channel_config (dit):
        dictionnaire avec les adresses des spots comme clés et
        le réglage des canaux comme valeurs
        (voir Spot.get_channels_config() pour plus
        d'informations)
        - activ_time (string):
        horodatage de l'activation du réseau (voir activate())

        See Also
        --------
        Spot.read_temp()
        Spot.get_temp()
        Spot.read_status()
        Spot.get_status()
        Spot.read_config()
        Spot.get_config()
        Spot.read_specs()
        Spot.get_specs()
        Spot.get_channels_config()

        """

        if what is None:
            what = []
        else:
            what = convert_tolistof(data=what, type_out=str, none_omit=True)
        if spots is None:
            spots = []
        else:
            spots = convert_tolistof(data=spots, type_out=int, none_omit=True)
        if len(spots) == 0:
            spots = list(self.spots.keys())
        out = {}
        if "available_colors" in what or (len(what) == 0):
            out['available_colors'] = self.available_color
        if "temp" in what or (len(what) == 0):
            out["temp"] = {}
            for add in spots:
                out["temp"][add] = self.spots[add].get_temp(update=update)
        if "status" in what or (len(what) == 0):
            out["status"] = {}
            for add in spots:
                out["status"][add] = self.spots[add].get_status(update=update)
        if "config" in what or (len(what) == 0):
            out["config"] = {}
            for add in spots:
                out["config"][add] = self.spots[add].get_config(update=update)
        if "specs" in what or (len(what) == 0):
            out["specs"] = {}
            for add in spots:
                out["specs"][add] = self.spots[add].get_specs(update=update)
        if "channels_config" in what or (len(what) == 0):
            out["channels_config"] = {}
            for add in spots:
                out["channels_config"][add] = self.spots[add].get_channels_config(update=update)
        if "activ_time" in what or (len(what) == 0):
            out['activ_time'] = self.activ

        return out

    def set_colors(self, colors, intensity, pwm_start=0, pwm_stop=1):
        """Envoie un réglage des canaux des spots en fonction de
        leur code couleur.

        Parameters
        ----------
        colors: list of string
            liste des codes couleurs des LED à régler.
            Si le caractère "*" est donné, toutes les
            couleurs sont sélectionnées.
        intensity: float
            intensité du courant exprimé en % du courant pic.
            Si l'overdirve est actif, l'intensité peut donc
            être exprimée jusque 200%.
        pwm_start: float
            instant d'allumage du canal durant le cyle PWM
            exprimé sur une échelle relative de 0 à 1
            (défaut: 0).
        pwm_stop: float
            instant d'extinction du canal durant le cyle PWM
            exprimé sur une échelle relative de 0 à 1
            (défaut: 1).

        Returns
        -------
        int
            0 (succès) ou 1 (erreur)

        See Also
        --------
        Spot.set_colors()
        """

        logger.debug("CALL")
        error = 0
        for spot in self.spots.values():
            error += spot.set_colors(colors=colors, intensity=intensity,\
                pwm_start=pwm_start, pwm_stop=pwm_stop)

        if error != 0:
            return 1
        return 0

    def shutdown(self, spots=None, colors="*"):
        """Ammène l'intensité du courant à 0 mA pour
        les LED sélectionnées.

        Parameters
        ----------
        spots : list
            liste des adresses des spots à contrôler.
        colors : list
            liste des codes couleur des canaux à éteindre.
            Si le caractère "*" (défaut) est donné, toutes les
            couleurs sont sélectionnées.

        Returns
        -------
        int
            0 (succès) ou 1 (erreur)

        See Also
        --------
        Spot.shutdown()

        """

        if spots is None:
            spots = []
        else:
            spots = convert_tolistof(data=spots, type_out=int, none_omit=False)
        if len(spots) == 0:
            spots = self.spots.keys()
        spots = list(set(spots) & set(list(self.spots.keys())))
        colors = convert_tolistof(data=colors, type_out=str, none_omit=False)

        if len(spots) == 0:
            logger.warning("Spot to shutdown not found")
            return 0

        if "*" in colors:
            colors = self.available_color
        colors = list(set(colors) & set(list(self.available_color)))
        if len(colors) == 0:
            logger.warning("Color to shutdown not found")
            return 0

        error = 0
        for add in spots:
            error += self.spots[add].shutdown(colors=colors)

        if error != 0:
            return 1
        return 0

    def write(self, spots=None):
        """Enregistre la configuration actuelle des spots
        sélectionnés comme configuration par défaut
        lancée à l'allumage.

        Parameters
        ----------
        spots : list
            liste des adresses des spots à traiter.

        Returns
        -------
        int
            0 (succès) ou 1 (erreur)

        Notes
        -----
        L'écriture dans la mémoire non volatile du spot
        est sujette à l'usure. Cette fonction est donc à appellé
        avec partimonie.
        """

        if spots is None:
            spots = []
        else:
            spots = convert_tolistof(data=spots, type_out=int, none_omit=True)
        if len(spots) == 0:
            spots = list(self.spots.keys())

        error = 0
        for add in spots:
            error += self.spots[add].write()

        if error != 0:
            return 1
        return 0

    def restart(self, spots=None):
        """Redémarre les spots sélectionnés
        comme lors d'une mise sous tension.

        Parameters
        ----------
        spots : list
            liste des adresses des spots à redémarrer.

        Returns
        -------
        int
            0 (succès) ou 1 (erreur)
        """
        if spots is None:
            spots = []
        else:
            spots = convert_tolistof(data=spots, type_out=int, none_omit=True)
        if len(spots) == 0:
            spots = list(self.spots.keys())

        error = 0
        for add in spots:
            error += self.spots[add].restart()

        if error != 0:
            return 1
        return 0

# ----------------------------------------------------------------------------
class Spot():
    """Classe pour la gestion des spots.
    Un spot regroupe un ensemble de canaux réglables de la classe 'Channel'.
    Un spot possède une configuration autre que celles des canaux
    (fréquence, allumé/éteind, maitre/esclave...).

    ...
    
    Attributes
    ----------
    add: int
        adresse du spot
    com: Com
        objet de la classe Com() pour la communication avec le luminaire
    firmvers: tuple
        version du firmware du spot
    specs: dict
        ensemble des spécifications du luminaire
    status: dict
        validité de la configuration et état des drivers
    temp: dict
        température du CPU et des cartes LED
    config: dict
        configuration du spot: fréquence, master/slave...
    ch_config: dict
        configuration des canaux
    channels: dict
        ensemble des objets de classe Channel géré par le spot,
        organisés par identifiant
    channels_bycolor: dict
        ensemble des objets de classe Channel géré par le spot,
        organisés par type de LED
    available_color: list
        liste des types de LED disponibles

    See Also
    --------
    read_specs(): structure de l'attribut 'specs'
    read_status: structure de l'attribut 'statut'
    read_temp: structure de l'attribut 'temp'
    read_config: structure de l'attribut 'config'
    """

    def __init__(self, address, com):
        """
        Parameters
        ----------
        address: int
            adresse du spot
        com: Com
            objet de la classe Com() pour la communication avec le luminaire
        """

        logger.debug("CALL")
        self.add = address
        self.com = com
        self.firmvers = (0, 0)
        self.specs = {
            'address': None,
            'firmware-version': None,
            'SN': None,
            'pcb-led': None,
            'timestamp': None
        }
        self.status = {
            'config': None,
            'drivers': {'global': None, 'descr' : None},
            'timestamp': None
        }
        self.temp = {
            'cpu': None,
            'pcb-led': None,
            'timestamp': None
        }
        self.config = {
            'master': None,
            'power': None,
            'freq': None,
            'overdrive': None
        }
        self.ch_config = {}
        self.channels = {}
        self.channels_bycolor = {}
        self.available_color = []

    def coms(self, cmd, arg=""):
        """Transmet une commande

        Parameters
        ----------
        cmd: str
            commande à envoyer
        arg: list
            arguments à ajouter après la commande

        Returns
        -------
        str
            réponse reçue (ou None en cas d'erreur)

        Notes
        -----
        Utilise la classe Com() et la méthode Com.send().
        L'adresse du spot est ajoutée en préfixe des arguments.

        See Also
        --------
        Com
        Com.send()
        """

        logger.debug("CALL")
        reply = self.com.send(cmd=cmd, prefix=self.add, \
            arg=arg, checksum=True)
        #print(reply)
        if str(reply['reply']).startswith("NAK"):
            logger.warning("Com. received code %s", reply['reply'])
        if reply['error'] is True:
            return None
        return reply['reply']

    def activate(self):
        """Lit l'ensemble des données de la mémoire et de l'état
        du spot et crèe les objet de classe Channel correspondant.

        Returns
        -------
        int
            0 (succès) ou 1 (erreur)

        Notes
        -----
        Appelle les fonctions suivantes:
        read_specs, read_status, read_config et build_channels

        See Also
        --------
        Spot.read_specs()
        Spot.read_status()
        Spot.read_config()
        Spot.build_channels()
        """

        logger.debug("CALL")
        error = 0
        error += self.read_specs()
        error += self.read_status()
        error += self.read_config()
        error += self.build_channels()

        if error != 0:
            return 1
        return 0

    def read_specs(self):
        """Charge les spécifications du spot

        Returns
        -------
        int
            0 (succès) ou 1 (erreur)

        Notes
        -----
        Charge les données dans la variable 'specs' (dict)
        selon la structure suivante:

        - timestamp: horodatage
        - address: adresse du spot (car si réponse, l'adresse est valide)
        - firmware-version: version du firmware du spot
        - SN: numéro de série du spot
        - pcb-led: dict (dupliqué sous les clés 0 et 1 pour chaque PCB)
            - type: type de carte LED montée *
            - SN: numéro de série de la carte LED *
            - desc: dict (descriptif détaillé)
                - crc: contrôle de redondance cyclique
                pour le contrôle des données
                - type: type de carte LED montée *
                - SN: numéro de série de la carte LED *
                - channels: dict (une clé par canal)
                    - i_peek: courant maximal pour la LED (mA)
                    - manuf: fabricant
                    - col: code couleur
                    - wl: longueur d'onde ou température de couleur
                    - code_col: identifiant de LED (composé de col_wl)

        (*) Les données 'type' et 'SN' des cartes LED sont répétées à deux niveaux
        du dictionnaire car issues de deux commandes différentes envoyées au
        spot.
        """

        logger.debug("CALL")
        error = 0
        self.specs['timestamp'] = datetime.now().strftime(date_format)
        # CPU
        logger.debug("Reading firmware version and SN")
        reply = self.coms("GI")
        if reply is not None:
            self.specs['address'] = self.add
            self.specs['firmware-version'] = reply.split(" ")[0]
            self.specs['SN'] = reply.split(" ")[1]
            self.firmvers = self.specs['firmware-version'].lower()
            self.firmvers = self.firmvers.replace("v", "")
            self.firmvers = tuple(map(int, (self.firmvers.split("."))))
        else:
            error = 1
            logger.error("Read SN/firmware version failed")
        # PCB Led
        # Lecture du type et numéro de série
        logger.debug("Reading PCB LED type and SN")
        reply = self.coms("GL")
        #@! implémenter l'absence de carte comme exception !
        if reply is not None:
            try:
                rspl = reply.split(" ")
                self.specs['pcb-led'] = {
                    0: {'type': int(rspl[0]), 'SN': str(rspl[1])},
                    1: {'type': int(rspl[2]), 'SN': str(rspl[3])},
                }
            except:
                error = 1
                logger.error("Read type/SN of PCB LED failed")
        else:
            error = 1
            logger.error("Read type/SN of PCB LED failed")
        # Lecture du descriptif détaillé
        if len(self.specs['pcb-led']) > 0:
            logger.debug("Reading PCB LED description")
            for key, value in self.specs['pcb-led'].items():
                value['desc'] = {}
                reply = self.coms(cmd="GB", arg=key)
                if reply is not None:
                    reply = reply.split(" ")[1]
                    # PCB
                    value['desc']['crc'], reply = decrypt_led(reply, 16, "hex")
                    value['desc']['type'], reply = decrypt_led(reply, 16, "int")
                    value['desc']['SN'], reply = decrypt_led(reply, 32, "hex")
                    # Canaux
                    value['desc']['channels'] = {}
                    i = 0
                    while reply != "":
                        channel = {}
                        ipeek, reply = decrypt_led(reply, 8, "int")
                        channel['i_peek'] = ipeek*5 # mA
                        channel['manuf'], reply = decrypt_led(reply, 8, "text")
                        col, reply = decrypt_led(reply, 16, "text")
                        channel['col'] = col[::-1]
                        channel['wl'], reply = decrypt_led(reply, 16, "int")
                        channel['code_col'] = str(channel['col'])+"_"+str(channel['wl'])
                        value['desc']['channels'][i] = channel
                        i += 1
        return error

    def get_specs(self, what=None, update=False, timestamp=True):
        """Renvoit les spécifications du spot

        Parameters
        ----------
        what: list
            liste des clés du dictionnaire à renvoyer
            (voir read_specs). Une liste vide induit le renvoie
            de l'ensemble des données disponibles.
        update: bool
            True (intérroge le spot avant renvoie)
            ou Fasle (défaut, renvoi sans mise à jour des données)
        timestamp: bool
            True (défaut, inclut l'horodatage dans le dictionnaire
            de sortie) ou False (retire l'horodatage)

        Returns
        -------
        dict
            ensemble des spécifications

        See Also
        --------
        Spot.read_specs(): pour une description de la structure
        du dictionnaire renvoyé.
        """

        logger.debug("CALL")
        if update:
            self.read_specs()
        if what is None:
            specs = []
        else:
            specs = convert_tolistof(data=what, type_out=str, none_omit=True)
        out = {}
        if len(specs) == 0:
            out = self.specs
        else:
            out = {}
            for param in specs:
                try:
                    out[param] = self.specs[param]
                except KeyError:
                    out[param] = None
        if timestamp:
            out['timestamp'] = self.specs['timestamp']

        return out

    def read_status(self):
        """Charge les statuts du spot

        Returns
        -------
        int
            0 (succès) ou 1 (erreur)

        Notes
        -----
        Charge les données dans la variable 'status' (dict)
        selon la structure suivante:

        - timestamp: horodatage
        - config: validité de la configuration et de la
        synchronisation de l'horloge.
        OK = valide, ERR = erreur dans la configuration du spot
        (par exemple si le spot a le rôle d'esclave mais n'a
        pas la même fréquence que le maître sur le réseau)
        - drivers (dict): statuts de l'esnemble des drivers
            - global:
            OK = tous les drivers sont fonctionnels,
            ERR = au moins un driver est hors service
            - drivers: dict.
            Statut pour chaque driver.
            La clé du dictionnaire donne l'identifiant du canal
            correspondant et la valeur donne le statut:
            1 = fonctionnel,
            0 = en erreur (circuit ouvert ou court-circuit)
        """

        logger.debug("CALL")
        error = 0
        self.status['timestamp'] = datetime.now().strftime(date_format)
        reply = self.coms("GE")
        if reply is not None:
            rspl = reply.split(" ")
            # Config
            self.status['config'] = rspl[0]
            # Drivers
            rspl[1] = rspl[1].replace("O", "0")
            rspl[1] = list(bin(int(rspl[1], 16))[2:])
            rspl[1] = list(map(int, rspl[1]))
            self.status['drivers'] = {
                'global' : None,
                'descr' : {}
            }
            try:
                if sum(rspl[1]) == len(rspl[1]):
                    self.status['drivers']['global'] = "OK"
                else:
                    self.status['drivers']['global'] = "ERR"
            except TypeError:
                self.status['drivers']['global'] = None
                error = 1
                logger.error("Compute drivers global status failed")
            i = 0
            for value in rspl[1]:
                try:
                    self.status['drivers']['descr'][i] = int(value)
                except ValueError:
                    self.status['drivers']['descr'][i] = None
                    error = 1
                    logger.error("Read of driver %s status failed", i)
                i += 1
        else:
            self.status['config'] = None
            self.status['drivers'] = {'global': None, 'descr' : None}
            error = 1
            logger.error("Status read failed")

        return error

    def get_status(self, what=None, update=False, timestamp=True):
        """Renvoit les status du spot

        Parameters
        ----------
        what: list
            liste des clés du dictionnaire à renvoyer
            (voir read_status). Une liste vide (défaut) induit le renvoie
            de l'ensemble des données disponibles.
        update: bool
            True (intérroge le spot avant renvoie)
            ou Fasle (défaut, renvoi sans mise à jour des données)
        timestamp: bool
            True (défaut, inclut l'horodatage dans le dictionnaire
            de sortie) ou False (retire l'horodatage)

        Returns
        -------
        dict
            ensemble des status disponibles

        See Also
        --------
        Spot.read_status(): pour une description de la structure
        du dictionnaire renvoyé.
        """

        logger.debug("CALL")
        if update:
            self.read_status()
        out = {}
        if timestamp:
            out['timestamp'] = self.status['timestamp']
        if what is None:
            what = []
        else:
            what = convert_tolistof(data=what, type_out=str, none_omit=True)
        if len(what) == 0:
            out = self.status
        else:
            out = {}
            for param in what:
                try:
                    out[param] = self.status[param]
                except KeyError:
                    out[param] = None

        return out

    def read_temp(self):
        """Charge les températures du spot (en degré Celsius).

        Returns
        -------
        int
            0 (succès) ou 1 (erreur)

        Notes
        -----
        Charge les données dans la variable 'temp' (dict)
        selon la structure suivante:

        - timestamp: horodatage
        - cpu: température du CPU
        - pcb-led: dict.
        La clé du dictionnaire donne l'identifiant de la carte
        et la valeur donne la température.
        """

        logger.debug("CALL")
        error = 0
        self.temp = {
            'cpu': None,
            'pcb-led': None,
            'timestamp': datetime.now().strftime(date_format)
        }
        reply = self.coms("GT")
        if reply is not None:
            rslp = reply.split(" ")
            if len(rslp) > 0:
                self.temp['cpu'] = rslp[0]
                self.temp['pcb-led'] = {}
                i = 0
                for temp in rslp[1:]:
                    try:
                        self.temp['pcb-led'][i] = float(temp)
                    except ValueError:
                        self.temp['pcb-led'][i] = None
                        error = 1
                        logger.error("Unable to convert temperature to float")
                    i += 1
            else:
                error = 1
                logger.error("Temperatures string is empty")
        else:
            error = 1
            logger.error("Temperatures read failed")

        return error

    def get_temp(self, what=None, update=False, timestamp=True):
        """Renvoit les températures du spot (en degré Celsius)

        Parameters
        ----------
        what: list
            liste des clés du dictionnaire à renvoyer
            (voir read_temp). Une liste vide (défaut) induit le renvoie
            de l'ensemble des données disponibles.
        update: bool
            True (intérroge le spot avant renvoie)
            ou Fasle (défaut, renvoi sans mise à jour des données)
        timestamp: bool
            True (défaut, inclut l'horodatage dans le dictionnaire
            de sortie) ou False (retire l'horodatage)

        Returns
        -------
        dict
            ensemble des températures disponibles/demandées

        See Also
        --------
        Spot.read_temp(): pour une description de la structure
        du dictionnaire renvoyé.
        """

        logger.debug("CALL")
        if update:
            self.read_temp()
        out = {}
        if timestamp:
            out['timestamp'] = self.temp['timestamp']
        if what is None:
            what = []
        else:
            what = convert_tolistof(data=what, type_out=str, none_omit=True)
        if len(what) == 0:
            out = self.temp
        else:
            out = {}
            for param in what:
                try:
                    out[param] = self.temp[param]
                except KeyError:
                    out[param] = None

        return out

    def read_config(self):
        """Charge la configuration du spot.

        Returns
        -------
        int
            0 (succès) ou 1 (erreur)

        Notes
        -----
        Charge les données dans la variable 'config' (dict)
        selon la structure suivante:

        - timestamp: horodatage
        - power (int):
            0 = les drivers sont éteints,
            1 = les drivers sont actifs.
        - master (int):
            0 = le spot a le rôle d'esclave sur le réseau,
            1 = le spot a le rôle de maître sur le réseau.
        - overdrive (int):
            0 = courant moyen limité à 50% du courant pic,
            1 = courant moyen limité à 75% du courant pic,
            2 = aucune limitation du courant.
        - freq (float):
            la fréquence de l'horloge du PWM (en Hz)
        """

        logger.debug("CALL")
        error = 0
        self.config['timestamp'] = datetime.now().strftime(date_format)
        # Lecture des paramètres globaux
        logger.debug("Reading general parameters")
        reply = self.coms("GF")
        if reply is not None:
            reply = reply.replace("O", "0") # erreur dans code C du luminaire
            rspl = list((bin(int(reply, 16))[2:]))
            rspl = list(map(int, rspl))
            rspl.reverse()
            template = [0, 0, 0, 0]
            for i in range(0, len(template)):
                try:
                    template[i] = rspl[i]
                except IndexError:
                    template[i] = 0
            self.config['power'] = template[0]
            self.config['master'] = template[1]
            self.config['overdrive'] = int(str(template[2])+str(template[3]), 2)
        else:
            error = 1
            logger.error("Parameters read failed")

        # Lecture de la fréquence du PWM
        logger.debug("Reading PWM frequency")
        reply = self.coms("GS")
        if reply is not None:
            try:
                self.config['freq'] = float(reply)/10
            except ValueError:
                error = 1
                logger.error("PWM frequency can't be converted to float")
        else:
            error = 1
            logger.error("PWM frequency read failed")

        return error

    def get_config(self, what=None, update=False, timestamp=True):
        """Renvoit la configuration du spot

        Parameters
        ----------
        what: list
            liste des clés du dictionnaire à renvoyer
            (voir read_config). Une liste vide (défaut) induit le renvoie
            de l'ensemble des données disponibles.
        update: bool
            True (intérroge le spot avant renvoie)
            ou Fasle (défaut, renvoi sans mise à jour des données)
        timestamp: bool
            True (défaut, inclut l'horodatage dans le dictionnaire
            de sortie) ou False (retire l'horodatage)

        Returns
        -------
        dict
            ensemble des paramètres de configuration demandés

        See Also
        --------
        Spot.read_config(): pour une description de la structure
        du dictionnaire renvoyé.
        """

        logger.debug("CALL")
        if update:
            self.read_config()
        out = {}
        if timestamp:
            out['timestamp'] = self.config['timestamp']
        if what is None:
            what = []
        else:
            what = convert_tolistof(data=what, type_out=str, none_omit=True)
        if len(what) == 0:
            out = self.config
        else:
            out = {}
            for param in what:
                try:
                    out[param] = self.config[param]
                except KeyError:
                    out[param] = None

        return out

    def set_config(self, config=None):
        """Envoit une nouvelle configuration au spot

        Parameters
        ----------
        config : dict
            dictionnaire ayant la même structure que l'attribut
            'config' de la classe (voir read_config).
            Si une clé est absente, la valeur actuelle est conservée.

        Returns
        -------
        int
            0 (succès) ou 1 (erreur)

        See Also
        --------
        Spot.read_config(): pour une description de la structure
        du dictionnaire à envoyer.

        Notes
        -----
        Appelle la fonction Spot.read_config() avant l'envoi.
        """

        logger.debug("CALL")
        if not isinstance(config, dict):
            logger.error("Argument 'config' must be of type 'dict'")
            return 1
        # Mise à jour de la config avant modification
        self.read_config()
        config_new = copy.deepcopy(self.config)
        # Encodage
        error = 0
        for key, value in config.items():
            try:
                float(value)
                int(value)
            except ValueError:
                error += 1
                logger.error("Value for '%s' can't be converted to int or float", key)
                continue
            if key == 'master':
                if int(value) not in [0, 1]:
                    error += 1
                    logger.error("Value for master not valid")
                else:
                    config_new['master'] = int(value)
            elif key == 'power':
                if int(value) not in [0, 1]:
                    error += 1
                    logger.error("Value for power not valid")
                else:
                    config_new['power'] = int(value)
            elif key == 'overdrive':
                if self.firmvers < (1, 4):
                    logger.warning("Overdrive not available with version %s",\
                        self.specs['firmware-version'])
                    config_new['overdrive'] = 0
                else:
                    if int(value) not in [0, 1, 2]:
                        error += 1
                        logger.error("Value for overdrive not valid")
                    else:
                        config_new['overdrive'] = int(value)
            elif key == "freq":
                config_new['freq'] = float(value)
            else:
                logger.warning("Parameters '%s' not recognized", key)
        try:
            bitmask = str(bin(config_new['overdrive'])[2:]).zfill(2)
            bitmask += str(config_new['master'])
            bitmask += str(config_new['power'])
            bitmask = int(bitmask, 2)
        except (ValueError, KeyError):
            error += 1
            bitmask = None
            logger.error("Unable to format config bitmask")
        # Envoie de la config
        logger.debug("Send config bitmask")
        reply = self.coms(cmd="SF", arg=bitmask)
        if reply == "ACK":
            logger.debug("Config successfully modified")
        else:
            error += 1
            logger.error("Config write failed")
        # Fréquence
        reply = self.coms(cmd="SS", arg=config_new['freq']*10)
        if reply == "ACK":
            logger.debug("Frequency successfully modified")
        elif reply == "NAK 4":
            error += 1
            logger.error("Frequency %s Hz not permitted", config_new['freq'])
        else:
            error += 1
            logger.error("Frequency write failed")

        if error != 0:
            return 1
        return 0

    def build_channels(self):
        """Construit les objets de classe Channel pour le spot

        Returns
        -------
        int
            0 (succès) ou 1 (erreur)

        Notes
        -----
        La fonction recherche les spécifications des canaux dans l'attribut
        'specs' et crèe un objet de la classe Channel pour chacun d'eux.

        Ces objets sont rassemblés dans deux dictionnaires:

        - Le premier organise les canaux par leur identifiant.
        La clé correspond à l'identifiant du canal et la valeur
        à l'objet de classe Channel.
        - Le second rassemble les objets de classe Channel associés
        à des LED de même type. Chaque entrée du dictionnaire
        a pour clé le code couleur du type de LED et pour valeur
        une liste des objets Channel.
        """

        logger.debug("CALL")
        try:
            if len(self.specs['pcb-led'][0]['desc']["channels"].keys()) > 0:
                pass
            else:
                logger.error("No channels description found")
                return 1
        except (KeyError, TypeError, AttributeError):
            logger.error("No channels description found")
            return 1

        for key, value in self.specs['pcb-led'][0]['desc']["channels"].items():
            channel = Channel(channel_id=key, address=self.add,\
                com=self.com, specs=value, spot_config=self.config)
            self.channels[key] = channel
            self.available_color.append(value['code_col'])
            try:
                self.channels_bycolor[value['code_col']].append(channel)
            except KeyError:
                self.channels_bycolor[value['code_col']] = [channel]
        self.available_color = list(set(self.available_color))
        return 0

    def get_available_colors(self):
        """Renvoi la liste des code couleur des LED disponibles.

        Returns
        -------
        list
            liste des code couleur (par exemple, [BL_460, HR_660])
        """

        logger.debug("CALL")
        return self.available_color

    def set_channels(self, channels, intensity, pwm_start=0, pwm_stop=1):
        """Envoit une nouvelle configuration aux cannaux
        appelés par leur identifiant.

        Parameters
        ----------
        channels : list
            liste des identifiants des canaux à contrôler.
            Si une valeur négative est donnée, tous les
            canaux du spot sont sélectionnés.
        intensity: float
            intensité du courant exprimé en % du courant pic.
            Si l'overdirve est actif, l'intensité peut donc
            être exprimée jusque 200%.
        pwm_start: float
            instant d'allumage du canal durant le cyle PWM
            exprimé sur une échelle relative de 0 à 1
            (défaut: 0).
        pwm_stop: float
            instant d'extinction du canal durant le cyle PWM
            exprimé sur une échelle relative de 0 à 1
            (défaut: 1).

        Returns
        -------
        int
            0 (succès) ou 1 (erreur)

        See Also
        --------
        Spot.set_colors()
        Channel
        Channel.set_config()

        Examples
        --------
        - canal 8 allumé à 50% en courant continu:
        set_channels(channels=[8], intensity=50, pwm_start=0, pwm_stop=1)

        - canaux 2, 3 et 10 allumés à 80% durant
        les trois derniers quarts du cycle PWM:
        set_channels(channels=[2, 3, 10], intensity=80.0, pwm_start=0.25, pwm_stop=1)

        - tous les canaux à 10% avec une rapport cyclique de 20%:
        set_channels(channels=-1, intensity=10.0, pwm_start=0, pwm_stop=0.2)
        """

        logger.debug("CALL")
        channels = convert_tolistof(data=channels, type_out=int, none_omit=True)
        if len(channels) == 0:
            return 1
        if min(channels) < 0:
            channels = list(self.channels.keys())
        channels = list(set(channels) & set(list(self.channels.keys())))
        error = 0
        for i in channels:
            error += self.channels[i].set_config(intensity=intensity, \
                start=pwm_start, stop=pwm_stop)
        if error != 0:
            return 1

        return 0

    def set_colors(self, colors, intensity, pwm_start=0, pwm_stop=1):
        """Envoit une nouvelle configuration aux cannaux
        appelés par leur code couleur.

        Parameters
        ----------
        colors : list
            liste des codes couleur des canaux à contrôler.
            Si le caractère "*" est donné, toutes les
            couleurs sont sélectionnées.
        intensity: float
            intensité du courant exprimé en % du courant pic.
            Si l'overdirve est actif, l'intensité peut donc
            être exprimée jusque 200%.
        pwm_start: float
            instant d'allumage du canal durant le cyle PWM
            exprimé sur une échelle relative de 0 à 1
            (défaut: 0).
        pwm_stop: float
            instant d'extinction du canal durant le cyle PWM
            exprimé sur une échelle relative de 0 à 1
            (défaut: 1).

        Returns
        -------
        int
            0 (succès) ou 1 (erreur)

        See Also
        --------
        Spot.set_channels()
        Channel

        Examples
        --------
        set_colors(colors=["BL_460", "YE_590"], intensity=18.0, pwm_start=0, pwm_stop=0.6)
        """

        logger.debug("CALL")
        colors = convert_tolistof(data=colors, type_out=str, none_omit=True)
        if "*" in colors:
            colors = self.available_color
        colors = list(set(colors) & set(self.channels_bycolor.keys()))
        error = 0
        for col in colors:
            for channel in self.channels_bycolor[col]:
                error += channel.set_config(intensity=intensity, start=pwm_start, stop=pwm_stop)
        if error != 0:
            return 1
        return 0

    def shutdown(self, channels=None, colors="*"):
        """Ammène l'intensité du courant à 0 mA sur les
        canaux sélectionnés.

        Parameters
        ----------
        channels : list
            liste des identifiants des canaux à contrôler.
            Si une valeur négative est donnée, tous les
            canaux du spot sont sélectionnés. Est prioritaire
            sur le paramètre 'colors'
        colors : list
            liste des codes couleur des canaux à contrôler.
            Si le caractère "*" (défaut) est donné, toutes les
            couleurs sont sélectionnées.

        Returns
        -------
        int
            0 (succès) ou 1 (erreur)

        See Also
        --------
        Channel
        Channel.shutdown()

        Examples
        --------
        shutdown(channels=[1,2,5,9])
        shutdown(colors=["WH_4000"])
        """

        logger.debug("CALL")
        channels = convert_tolistof(data=channels, type_out=int, none_omit=True)
        colors = convert_tolistof(data=colors, type_out=str, none_omit=True)
        if len(channels) != 0:
            error = self.set_channels(channels=channels, intensity=0, pwm_start=0, pwm_stop=0)
        else:
            error = self.set_colors(colors=colors, intensity=0, pwm_start=0, pwm_stop=0)
        return error

    def get_channels_config(self, channels=-1, what=None, update=False, timestamp=True):
        """Renvoit la configuration des canaux sélectionnés

        Parameters
        ----------
        channels: list
            liste des identifiants des canaux à intérroger.
            Si une valeur négative est donnée, tous les
            canaux du spot sont sélectionnés.
        what: list
            liste des clés du dictionnaire à renvoyer
            (voir la méthode 'read_config' de la classe Channel).
        update: bool
            True (intérroge le spot avant renvoie)
            ou Fasle (défaut, renvoi sans mise à jour des données)
        timestamp: bool
            True (défaut, inclut l'horodatage dans le dictionnaire
            de sortie) ou False (retire l'horodatage)

        Returns
        -------
        dict
            ensemble des paramètres de configuration demandés.
            la clé de premier niveau correspond à l'identifiant du canal.
        """

        logger.debug("CALL")
        channels = convert_tolistof(data=channels, type_out=int, none_omit=True)
        out = {}
        if len(channels) == 0:
            return out
        if min(channels) < 0:
            channels = list(self.channels.keys())
        for i in channels:
            try:
                out[i] = self.channels[i].get_config(what=what, update=update, timestamp=timestamp)
            except KeyError:
                out[i] = None

        return out

    def write(self):
        """Enregistre la configuration actuelle du spot comme
        configuration par défaut lancée à l'allumage.

        Returns
        -------
        int
            0 (succès) ou 1 (erreur)

        Notes
        -----
        L'écriture dans la mémoire non volatile du spot
        est sujette à l'usure. Cette fonction est donc à appellé
        avec partimonie.
        """

        logger.debug("CALL")
        reply = self.coms("SV")
        if reply != "ACK":
            return 1
        return 0

    def restart(self):
        """Redémarre le spot comme lors du mise sous tension.

        Returns
        -------
        int
            0 (succès) ou 1 (erreur)
        """

        logger.debug("CALL")
        reply = self.coms("RST")
        if reply != "ACK":
            return 1
        return 0

# ----------------------------------------------------------------------------
class Channel():
    """Classe pour la gestion des cannaux.
    Un canal regroupe un seul type de LED qui partagent un même réglage.

    ...

    Attributes
    ----------
    channel_id: int
        Identifiant
    address: int
        adresse du spot
    com: Com
        objet de la classe Com() pour la communication avec le luminaire
    specs: dict
        spécifications (courant max, type de LED, ...)
    spot_config: dict
        configuration du spot (voir classe Spot())
    """

    def __init__(self, channel_id, address, com, specs, spot_config):
        """
        Parameters
        ----------
        channel_id: int
            Identifiant
        address: int
            adresse du spot
        com: Com
            objet de la classe Com() pour la communication avec le luminaire
        specs: dict
            spécifications (courant max, type de LED, ...)
        spot_config: dict
            configuration du spot (voir classe Spot())
        """

        logger.debug("CALL")
        self.ch_id = channel_id
        self.add = address
        self.com = com
        self.specs = specs
        self.spot_config = spot_config
        self.config = {
            'current': None,
            'intensity': None,
            'pwm_start': None,
            'pwm_stop': None,
            'timestamp': None
        }

    def coms(self, cmd, arg=""):
        """Transmet une commande

        Parameters
        ----------
        cmd: str
            commande à envoyer
        arg: list
            arguments à ajouter après la commande

        Returns
        -------
        str
            réponse reçue (ou None en cas d'erreur)

        Notes
        -----
        Utilise la classe Com() et sa méthode send().
        Le préfixe est ajouté en fonction des paramètres de l'objet
        (adresse et identifiant).

        See Also
        --------
        Com
        Com.send()
        """

        logger.debug("CALL")
        reply = self.com.send(cmd=cmd, prefix=[self.add, self.ch_id], arg=arg, checksum=True)
        #print(reply)
        if str(reply['reply']).startswith("NAK"):
            logger.warning("Com. received code %s", reply['reply'])
        if reply['error'] is True:
            return None
        return reply['reply']

    def read_config(self):
        """Lit la configuration du canal

        Returns
        -------
        int
            0 (succès) ou 1 (erreur)

        Notes
        -----
        Sauve la réponse dans la variable 'config' de la classe.
        Clé du dictionnaire de la variable 'config':

        - current: courant tranversant le canal (mA)
        - intensity: intensité du courant pic (en %)
        - pwm_start: point de départ du cyle PWM (0-1)
        - pwm_start: point d'arrêt du cyle PWM (0-1)
        - timestamp: horodatage de la lecture
        """

        logger.debug("CALL")
        error = 0
        self.config['timestamp'] = datetime.now().strftime(date_format)
        reply = self.coms("GC")
        if reply is not None:
            rspl = reply.split(" ")
            if len(rspl) == 4:
                self.config['current'] = int(rspl[1])*(1140/200)
                self.config['intensity'] = 100*int(rspl[1])*(1140/200)/self.specs['i_peek']
                self.config['pwm_start'] = int(rspl[2])/200
                self.config['pwm_stop'] = int(rspl[3])/200
            else:
                error = 1
                logger.error("Read configuration of channel %s:%s failed",\
                    self.add, self.ch_id)
        else:
            error = 1
            logger.error("Read configuration of channel %s:%s failed",\
                self.add, self.ch_id)
        return error

    def get_config(self, what=None, update=False, timestamp=True):
        """Renvoi la configuration du canal.

        Parameters
        ----------
        what: list
            clés de la configuration à renvoyer.
            Si la liste est vide (défaut), l'ensemble des données est renvoyée.
        update: bool
            si True, actualise les données (appelle read_config). Défaut: False.
        timestamp: bool
            si True (defaut) renvoie aussi le timestamp de la dernière lecture.

        Returns
        -------
        dict
            configuration du canal.
            Voir la méthode read_config pour les données disponibles.
        """

        logger.debug("CALL")
        if update:
            self.read_config()
        out = {}
        if timestamp:
            out['timestamp'] = self.config['timestamp']
        if what is None:
            what = []
        else:
            what = convert_tolistof(data=what, type_out=str, none_omit=True)
        if len(what) == 0:
            out = self.config
        else:
            out = {}
            for param in what:
                try:
                    out[param] = self.config[param]
                except KeyError:
                    out[param] = None

        return out

    def set_config(self, intensity, start=0, stop=1):
        """Envoi une configuration au canal.

        Parameters
        ----------
        intensity: float
            intensité du courrant en % du courant pic
        start: float
            point de départ du cyle PWM (0-1)
        stop: float
            point d'arrêt du cyle PWM (0-1)

        Returns
        -------
        int
            0 (succès) ou 1 (erreur)
        """

        logger.debug("CALL")
        # Vérification
        try:
            intensity = float(intensity)
            start = float(start)
            stop = float(stop)
        except ValueError:
            logger.error("Arguments must be floats")
            return 1
        # PWM
        if start < 0 or start > 1:
            start = 0
            logger.warning("PWM start point is not valid and is replaced by '0'")
        if stop < 0 or stop > 1:
            stop = 1
            logger.warning("PWM stop point is not valid and is replaced by '1'")
        pwm_start = int(start*200)
        pwm_stop = int(stop*200)
        # Courant
        if start == stop or intensity < 0:
            current_step = int(0)
        else:
            current_max = self.specs['i_peek'] # mA
            current_max = 0.494*current_max/abs(stop-start) # par défaut 50% du courant pic
            current = intensity*current_max/100
            current_step = int(200*(current/1140))
            if current_step > 200:
                current_step = int(200)

        # Envoi
        reply = self.coms("SC", arg=[current_step, pwm_start, pwm_stop])
        if reply != "ACK":
            return 1
        return 0

    def shutdown(self):
        """Eteint le canal.

        Returns
        -------
        int
            0 (succès) ou 1 (erreur)

        Notes
        -----
        Equivalent à set_config(intensity=0, start=0, stop=0)
        """

        logger.debug("CALL")
        reply = self.coms("SC", arg=[0, 0, 0])
        if reply != "ACK":
            return 1
        return 0
        